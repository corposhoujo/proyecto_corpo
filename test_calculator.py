import unittest
from calculator import Calculator

class TestCalculatorAdd(unittest.TestCase):
        def test_add_positive(self):
            result = Calculator.add(1,100)
            self.assertEqual(result, 101)
        def test_add_negative(self):
            result = Calculator.add(-1,-100)
            self.assertEqual(result, -101)
        def test_add_string(self):
            with self.assertRaises(TypeError):
                    Calculator.add(1,"a")

class TestCalculatorSubtract(unittest.TestCase):
        def test_subtract_positive(self):
            result = Calculator.subtract(1,100)
            self.assertEqual(result, -99)
        def test_subtract_negative(self):
            result = Calculator.subtract(-1,-100)
            self.assertEqual(result, 99)
        def test_subtract_string(self):
            with self.assertRaises(TypeError):
                    Calculator.add(1,"a")

class TestCalculatorMultiply(unittest.TestCase):
        def test_multiply_positive(self):
            result = Calculator.multiply(1,100)
            self.assertEqual(result, 100)
        def test_multiply_negative(self):
            result = Calculator.multiply(-1,-100)
            self.assertEqual(result, 100)
        def test_multiply_string(self):
            with self.assertRaises(TypeError):
                    Calculator.add(1,"a")
            
class TestCalculatorDivide(unittest.TestCase):
        def test_divide_positive(self):
            result = Calculator.divide(4,2)
            self.assertEqual(result, 2)
        def test_divide_byzero(self):
            with self.assertRaises(ValueError):
                Calculator.divide(10, 0)

            

if __name__ == '__main__':
    unittest.main()
