class Calculator:
    
    def add(x, y):
        if not type(x) is int:
            raise TypeError('Only integers are allowed!')
        if not type(y) is int:
            raise TypeError('Only integers are allowed!')
        return x + y

    def subtract(x, y):
        if not type(x) is int:
            raise TypeError('Only integers are allowed!')
        if not type(y) is int:
            raise TypeError('Only integers are allowed!')
        return x - y
     
    def multiply(x, y):
        if not type(x) is int:
            raise TypeError('Only integers are allowed!')
        if not type(y) is int:
            raise TypeError('Only integers are allowed!')
        return x * y
     
    def divide(x, y):
        if y == 0:
            raise ValueError('Can not divide by zero!')
        return x / y
